CREATE TABLE person(
   person_id INT ,
   person_name varchar(100) ,
   health_status varchar(100) ,
   confirmed_status_date  timestamp,
   PRIMARY KEY(person_id)
);


CREATE TABLE place(
   place_id INT ,
   place_name varchar(100) ,
   place_type varchar(100) ,
   PRIMARY KEY(place_id)
);

CREATE TABLE visit(
   visit_id INT ,
   person_id INT ,
   place_id int ,
   start_datetime  timestamp,
   end_datetime timestamp,
   PRIMARY KEY(visit_id),
   CONSTRAINT fk_table
      FOREIGN KEY(person_id) 
	  REFERENCES person(person_id),
      FOREIGN KEY(place_id) 
	  REFERENCES place(place_id)	
);



select *  from person ; 
select * from visit ;
select * from place ;

--ON VEUT LES NOMS PLUS STATUS DE SANTE DES PERSONNES QUE LADYN GREER A CROISE (MEME LIEU, MEME MOMENT)
select * from person where person_name = 'Landyn Greer' ;


-- il me faut les lieux où ladyn greer a été  dans la table visit 
select * from visit where person_id = 1 ; 


-- tous ceux qui ont été à la place numero 33 
select person_id, place_id, start_datetime from visit where place_id = 33 ;



-- on effectue une auto jointure sur la table visit pour trouver les individus qui ont visité le même endroit que ladyn geer. C'est à dire la place n°33 et la place n°88
select distinct v1.visit_id , v1.person_id , v1.place_id , v1.start_datetime , v1.end_datetime  from 
visit as v1 inner join visit as v2  on v1.place_id in (33,88) order by person_id asc ;

-- on s'interesse dans cette auto jointure de la table visit à la place n°33. On trouve par la requete qui suit, tous les individus ayant visité la place n°33 
select distinct v1.visit_id ,  v1.person_id ,  v1.place_id ,  v1.start_datetime , v1.end_datetime  from 
visit as v1 inner join visit as v2  on v1.place_id = 33 order by person_id asc;

-- la requete suivante permet d'obtenir la liste des individus ayant visité la place n°88 tout comme ladyn greer
select distinct v1.visit_id ,  v1.person_id ,  v1.place_id ,  v1.start_datetime , v1.end_datetime  from 
visit as v1 inner join visit as v2  on v1.place_id = 88 order by person_id asc;


-- la requete suivante donne comme résultat la liste des individus ayant visité la place n°33 le jour où ladyn greer a visité la place n°33. Ex : ladyn greer a commencé la visite de la place 33 le 2020-10-07 à 20h54 et a terminé sa visite le 20-10-08 à 03h44. 
--Tandis que Enzo Eaton a commencé la sienne le 2020-10-07 à 12h29 pour terminer le 2020-10-07 à 22h02. On peut en déduire que Enzo Eaton et Ladyn greer ce sont rencontrés entre 20:54 et 22:02 le 2020-10-07.
select * from ( select distinct v1.visit_id ,  v1.person_id ,  v1.place_id ,  v1.start_datetime , v1.end_datetime  from  
visit as v1 inner join visit as v2  on v1.place_id = 33 ) as matable  where start_datetime between '2020-10-07 00:00' and '2020-10-07 23:59'  ;

-- je renomme les colonnes de la requete précédente afin d'effectuer une jointure avec la table person
select * from ( select distinct v1.visit_id as ma_visit ,  v1.person_id  as ma_person,  v1.place_id as ma_place,  v1.start_datetime as date_debut, v1.end_datetime as date_fin  from  
visit as v1 inner join visit as v2  on v1.place_id = 33 ) as matable  where date_debut between '2020-10-07 00:00' and '2020-10-07 23:59'  ;

-- A partir de la table précédente je veux les noms et statuts de santé de ces personnes. on fera une jointure avec la table person 

select p.person_id, p.person_name, p.health_status, ma_place, date_debut, date_fin from 
(select * from ( select distinct v1.visit_id as ma_visit ,  v1.person_id  as ma_person,  v1.place_id as ma_place,  v1.start_datetime as date_debut, v1.end_datetime as date_fin  from  
visit as v1 inner join visit as v2  on v1.place_id = 33 ) as matable  where date_debut between '2020-10-07 00:00' and '2020-10-07 23:59' ) as matable12, person p where p.person_id = ma_person 


-- il faut maintenant effectuer les mêmes requetes pour la place 88. 

--la requete suivante donne comme résultat la liste des individus ayant visité la place n°88 le jour où ladyn greer a visité la place n°88. Ex : ladyn greer a commencé la visite de la place 88 le 2020-09-26 à 18h28 et a terminé sa visite le 20-09-26 à 21h40. 
--Tandis que Juliana DOYLE a commencé la sienne le 2020-09-26 à 14H28 pour terminer le 2020-09-26 à 21h38. On peut en déduire que Juliana Doyle  et Ladyn greer ce sont rencontrés entre 18h28 et 21:38 le 2020-09-26.
select * from(
select distinct v1.visit_id ,  v1.person_id ,  v1.place_id ,  v1.start_datetime , v1.end_datetime  from 
visit as v1 inner join visit as v2  on v1.place_id = 88 order by person_id asc) as matable2 where start_datetime between '2020-09-26 00:00' and '2020-09-26 23:59' ;

-- je renomme les colonnes
select * from(
select distinct v1.visit_id as la_visit,  v1.person_id as la_person ,  v1.place_id as la_place ,  v1.start_datetime as la_datedebut , v1.end_datetime as la_datefin  from 
visit as v1 inner join visit as v2  on v1.place_id = 88 order by la_person asc) as matable2 where la_datedebut between '2020-09-26 00:00' and '2020-09-26 23:59' ;

-- j'effectue une jointure avec la table person 
select p.person_id, p.person_name, p.health_status, la_place, la_datedebut, la_datefin from (select * from(select distinct v1.visit_id as la_visit,  v1.person_id as la_person ,  v1.place_id as la_place ,  v1.start_datetime as la_datedebut , v1.end_datetime as la_datefin  from 
visit as v1 inner join visit as v2  on v1.place_id = 88 order by la_person asc) as matable2 where la_datedebut between '2020-09-26 00:00' and '2020-09-26 23:59' ) as matable3, person p where p.person_id = la_person ;


-- ON VEUT MAINTENANT LE NOMBRE DE MALADES CROISES DANS UN BAR AU MEME MOMENT


-- Je commence par les requetes suivantes :
select * from place where place_type = 'Bar' ;
select *  from visit ;
-- j'effectue une jointure des deux tables précédentes pour avoir comme résultat les personnes qui sont allé dans un bar
-- la personne avec l'identifiant 3 (Lindsay Melton) person_id = 3  est allé dans un bar à 2 reprises et à des dates différentes. 
-- premièrement Lindsay Melton est allé dans un bar le 2020-10-03 à 23h57 qu'elle a quitté le 2020-10-04 à 12:19. 
-- deuxièmement Lindsay Melton est allé dans un bar le 2020-10-07 à 00h22 qu'elle a quitté le 2020-10-07 à 13h44 
-- Parmi nos individus certains sont allés plusieurs fois dans un bar. 

select v.person_id, v.place_id, v.start_datetime, v.end_datetime, p.place_name, p.place_type   from visit v, place p where v.place_id = p.place_id  and p.place_type =  'Bar' order by v.person_id asc; 

-- je renomme les colonnes de la requete précédente
select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p where v.place_id = p.place_id  and p.place_type =  'Bar' order by v.person_id asc; 

-- j'effectue une jointure de la table précédente avec la table person
-- il faut que la date à laquelle l'individu a été établi malade soit inférieure à la date d'entrée dans le bar. Sinon aucun individu déclaré malade n'est allé dans un bar. 
-- on peut voir aussi la différence de jours qui s'est écoulée entre le moment d'entrée dans le bar et la date à laquelle l'individu est déclarée malade 

select * from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p where v.place_id = p.place_id  and p.place_type =  'Bar' order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and p.health_status = 'Sick' and p.confirmed_status_date < une_datedebut; 


-- pour les personnes en bonne santé qui sont allées dans un bar 
select * from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p where v.place_id = p.place_id  and p.place_type =  'Bar' order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and p.health_status = 'Healthy'; 



--Effectuons la différence des dates confirmed_status_date et une_datedebut 
-- Madame Taylor Luna fut déclarée malade 5 jours avant qu'elle ne rentre dans un bar le 2020-09-18 à 01:26:00
-- Monsieur Maurice Pruitt fut déclaré malade le 2020-09-15 à 02h40 et 22 jours ce sont écoulés entre le moment où il fut déclaré malade et le moment où il rentra dans le bar le 2020-10-07 à 18h29 peut-on considérer que maurice fut malade lorsqu'il rentra dans le bar 

select une_personne, une_place, une_datedebut, une_datefin, nom_deplace, type_deplace, person_id, person_name,health_status, confirmed_status_date, (une_datedebut - confirmed_status_date) as nombre_de_jour 
from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p where v.place_id = p.place_id  and p.place_type =  'Bar' order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and p.health_status = 'Sick' and p.confirmed_status_date < une_datedebut; 

-- Pour répondre à la question on va compter le nombre de personne ayant été déclaré malade et qui sont allés dans un bar.
-- la requete ci dessous donne comme résultat, un nombre total de 127. Ainsi 127 personnes fut déclarée malade lorsqu'elles sont allées dans un bar
select count(*) as nombre_de_malades from
( select une_personne, une_place, une_datedebut, une_datefin, nom_deplace, type_deplace, person_id, person_name,health_status, confirmed_status_date, (une_datedebut - confirmed_status_date) as nombre_de_jour 
from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p where v.place_id = p.place_id  and p.place_type =  'Bar' order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and p.health_status = 'Sick' and p.confirmed_status_date < une_datedebut ) as une_table_query  ;

-- On veut maintenant le nombre de personnes que Taylor Luna a croisé  
select * from person where person_name = 'Taylor Luna'

select * from visit where person_id = 4 ; 

-- on effectue une auto jointure sur la table visit pour trouver les individus qui ont visité le même endroit que Taylor Luna. C'est à dire les places n° 72 ,8,84,26,5,29,90,52
select distinct v1.visit_id , v1.person_id , v1.place_id , v1.start_datetime , v1.end_datetime  from 
visit as v1 inner join visit as v2  on v1.place_id in (72,8,84,26,5,29,4,52, 90) order by person_id asc ;


-- la requete suivante donne comme résultat la liste des individus ayant visité la place n°72 le jour où Taylor Luna a visité la place n°72. Ex : Taylor Luna a commencé la visite de la place 72 le 2020-09-23 à 05h51 et a terminé sa visite le 2020-09-23 à 11h05. 
--Tandis que Damien Underwood (person_id = 365) a commencé la sienne le 2020-09-23 à 10h58 pour terminer le 2020-09-23 à 23h07. On peut en déduire que Taylor Luna et Damien Underwood ce sont rencontrés entre 10h58 et 11h05 le 2020-09-23.
select * from (select distinct v1.visit_id ,  v1.person_id ,  v1.place_id ,  v1.start_datetime , v1.end_datetime  from  
visit as v1 inner join visit as v2  on v1.place_id = 72 )  as matable  where start_datetime between '2020-09-23 00:00' and '2020-09-23 12:00' order by person_id asc ;


-- A partir de la table précédente je veux les noms des personnes que Taylor Luna a croisé. on fera une jointure avec la table person 

select p.person_id, p.person_name, ma_place, date_debut, date_fin from 
(select * from ( select distinct v1.visit_id as ma_visit ,  v1.person_id  as ma_person,  v1.place_id as ma_place,  v1.start_datetime as date_debut, v1.end_datetime as date_fin  from  
visit as v1 inner join visit as v2  on v1.place_id = 72 ) as matable  where date_debut between '2020-09-23 00:00' and '2020-09-23 12:00' ) as matable12, person p where p.person_id = ma_person order by p.person_id ;

-- on repete le même processus pour les différentes places 
-- la place numero 8 
select * from (select distinct v1.visit_id ,  v1.person_id ,  v1.place_id ,  v1.start_datetime , v1.end_datetime  from  
visit as v1 inner join visit as v2  on v1.place_id = 8 )  as matable order by person_id ;

-- la requete suivante doit donné la liste des individus ayant visté la place 8 le meme jour que Taylor Luna 
-- En ce qui concerne la place 8 aucun individu n'a visité ce lieu au même moment. La requete suivante le prouve.
-- Taylor Luna a débuté la visite de la place 8 2020-09-18 à 23h37 et à terminé la visite  le 2020-09-19 à 05h52. Or tous ceux qui ont visité la place 8 ont débuté après la fin de visite de Taylor Luna
select p.person_id, p.person_name, ma_place, date_debut, date_fin from 
(select * from ( select distinct v1.visit_id as ma_visit ,  v1.person_id  as ma_person,  v1.place_id as ma_place,  v1.start_datetime as date_debut, v1.end_datetime as date_fin  from  
visit as v1 inner join visit as v2  on v1.place_id = 8 ) as matable  where date_debut between '2020-09-18 00:00' and '2020-09-19 23:59' ) as matable12, person p where p.person_id = ma_person order by p.person_id ;

-- On peut procéder ainsi pour déterminer l'ensemble des personnes ayant visité les mêmes lieux que Taylor luna au même moment. 


--On veut maintenant le nombre de malades par endroit (place_name) + durée moyennes de leurs visites dans cet endroit 
select * from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p 
where v.place_id = p.place_id   order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and health_status = 'Sick'
and p.confirmed_status_date < une_datedebut
order by type_deplace; 

-- il nous faut rajouter à la requete précédente le temps mis dans le lieu. C'est à dire la différence entre une_datefin  et une_datedebut 

select une_personne, une_place, une_datedebut, une_datefin, type_deplace, person_name, health_status, confirmed_status_date, (une_datefin - une_datedebut) as temps_ecoule from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p 
where v.place_id = p.place_id   order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and health_status = 'Sick'
and p.confirmed_status_date < une_datedebut
order by type_deplace; 

-- ensuite pour chaque type de place il faut on veut la duree moyenne et le nombre de personne qui ont fréquenté cet endroit 

select type_deplace, count (*) as nb_malades, avg(temps_ecoule) as moyenne_heure from 
(select une_personne, une_place, une_datedebut, une_datefin, type_deplace, person_name, health_status, confirmed_status_date, (une_datefin - une_datedebut) as temps_ecoule from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p 
where v.place_id = p.place_id   order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and health_status = 'Sick'
and p.confirmed_status_date < une_datedebut
order by type_deplace ) as la_table_query
group by type_deplace; 


-- On veut le nombre de sains (non malades) par endroit (place_name) + durée moyenne de leurs visites dans cet endroit 

select * from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p 
where v.place_id = p.place_id   order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and health_status = 'Healthy'
and p.confirmed_status_date < une_datedebut
order by type_deplace; 

-- il nous faut rajouter à la requete précédente le temps mis dans le lieu. C'est à dire la différence entre une_datefin  et une_datedebut 

select une_personne, une_place, une_datedebut, une_datefin, type_deplace, person_name, health_status, confirmed_status_date, (une_datefin - une_datedebut) as temps_ecoule from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p 
where v.place_id = p.place_id   order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and health_status = 'Healthy'
and p.confirmed_status_date < une_datedebut
order by type_deplace; 

-- ensuite pour chaque type de place il faut on veut la duree moyenne et le nombre de personne qui ont fréquenté cet endroit 

select type_deplace, count (*) as nb_malades, avg(temps_ecoule) as moyenne_heure from 
(select une_personne, une_place, une_datedebut, une_datefin, type_deplace, person_name, health_status, confirmed_status_date, (une_datefin - une_datedebut) as temps_ecoule from (select v.person_id as une_personne, v.place_id as une_place, v.start_datetime as une_datedebut, v.end_datetime as une_datefin, p.place_name as nom_deplace, p.place_type  as type_deplace from visit v, place p 
where v.place_id = p.place_id   order by v.person_id asc)
as unetable, person p where p.person_id = une_personne 
and health_status = 'Healthy'
and p.confirmed_status_date < une_datedebut
order by type_deplace ) as la_table_query
group by type_deplace; 